﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Checkpoint : MonoBehaviour
    {
        public int CheckpointNumber;
        public SpriteRenderer CheckFlag;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                PlayerController player = collision.gameObject.GetComponent<PlayerController>();

                player.SetNewCheckpoint(CheckpointNumber, transform.position, CheckFlag);
                
            }
        }
    }
}
