﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class EndScreen : MonoBehaviour
    {
        public Text TimeTaken;
        public Text TimesRestored;

        public void Start()
        {
            TimeTaken.text = string.Format("You took {0:0.##} seconds!", Stats.GetCompletionTime());
            TimesRestored.text = string.Format("You restored {0} times!", Stats.TimesRestored);
        }
    }
}
