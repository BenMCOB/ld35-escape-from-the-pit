﻿using UnityEngine;

namespace Assets.Scripts
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform Target;
        private Vector3 _newPosition = Vector3.zero;

        // ReSharper disable once UnusedMember.Local
        public void Start()
        {
            _newPosition = Target.position;
        }

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        public void Update()
        {
            int minRange = -1;
            int maxRange = 1;

            if ((Target.position.x - transform.position.x) > maxRange)
            {
                _newPosition.x = Target.position.x - maxRange;
            }
            else if ((Target.position.x - transform.position.x) < minRange)
            {
                _newPosition.x = Target.position.x - minRange;
            }

            _newPosition.y = Target.position.y;
            _newPosition.z = Target.position.z - 10;
            
            transform.position = _newPosition;
        }
    }
}
