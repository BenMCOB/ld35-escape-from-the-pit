﻿using System;
using UnityEngine;

namespace Assets.Scripts
{
    public class PitBoss : MonoBehaviour
    {
        public PlayerController Player;
        private Rigidbody2D _rigidbody;
        private readonly Vector3 _normalSpeed = new Vector2(0.05f, 0);
        private const int SMASH_FACTOR = 1200;

        public AudioClip PitBossHurtSfx;
        public AudioClip PlayerHurtSfx;

        public void Start()
        {
            _rigidbody = GetComponent<Rigidbody2D>();
        }
        
        public void Update()
        {
            Move();
            CheckWinCondition();
        }

        private void CheckWinCondition()
        {
            if (transform.position.y < -71)
            {
                Stats.EndGame();
                Application.LoadLevel("EndScreen");
            }
        }

        private void Move()
        {
            if (Player.transform.position.x > 1.5f)
            {
                if (transform.position.x > -7.2)
                {
                    if (transform.position.x + (_normalSpeed.x * -1) > -7.2)
                    {
                        transform.position += _normalSpeed * -1;
                    }
                    else
                    {
                        transform.position = new Vector3(-7.2f, transform.position.y, 0);
                    }
                }
                else
                {
                    if (transform.position.x + (_normalSpeed.x) < -7.2)
                    {
                        transform.position += _normalSpeed;
                    }
                    else
                    {
                        transform.position = new Vector3(-7.2f, transform.position.y, 0);
                    }
                }
                return;
            }

            Vector3 horizontalMotion;
            
            if (PlayerToTheRight())
            {
                // Move Right
                horizontalMotion = _normalSpeed;
            }
            else
            {
                horizontalMotion = _normalSpeed * -1;
            }

            if (horizontalMotion.x > 0 && _rigidbody.velocity.x < 0)
            {
                horizontalMotion = CalculateVelocityChange(horizontalMotion);
            }
            else if (horizontalMotion.x < 0 && _rigidbody.velocity.x > 0)
            {
                horizontalMotion = CalculateVelocityChange(horizontalMotion);
            }

            transform.position += horizontalMotion;
        }

        private Vector3 CalculateVelocityChange(Vector3 rightMotion)
        {
            float difference = _rigidbody.velocity.x;
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x + rightMotion.x, _rigidbody.velocity.y);

            rightMotion = Math.Abs(rightMotion.x) > Math.Abs(difference)
                ? new Vector3(rightMotion.x + difference, 0f)
                : Vector3.zero;
            return rightMotion;
        }

        private bool PlayerToTheRight()
        {
            return Player.transform.position.x > transform.position.x;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                if (Player.AbleToSmash(collision))
                {
                    // Push back PitBoss
                    if (PlayerToTheRight())
                    {
                        _rigidbody.AddForce(Vector2.left * SMASH_FACTOR * 2);
                        _rigidbody.AddForce(Vector2.up * SMASH_FACTOR);
                    }
                    else
                    {
                        _rigidbody.AddForce(Vector2.right * SMASH_FACTOR * 2);
                        _rigidbody.AddForce(Vector2.up * SMASH_FACTOR);
                    }

                    AudioSource.PlayClipAtPoint(PitBossHurtSfx, transform.position);
                }
                else
                {
                    // Throw player
                    Player.RigidBody.AddForce(Vector2.up * 0.2f);
                    Player.RigidBody.AddForce(Vector2.right * 0.4f);

                    AudioSource.PlayClipAtPoint(PlayerHurtSfx, Player.transform.position);
                }
            }
        }

        public void OnCollisionStay2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                // Throw player
                Player.RigidBody.AddForce(Vector2.up * 0.05f);
            }
        }

    }
}
