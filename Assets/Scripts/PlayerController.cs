﻿using System;
using System.Collections;
using UnityEngine;

namespace Assets.Scripts
{
    public class PlayerController : MonoBehaviour
    {
        private readonly Vector2 _normalSpeed = new Vector2(0.1f, 0);
        private readonly Vector2 _circleSpeed = new Vector2(0.15f, 0);
        private SpriteRenderer _spriteRenderer;
        private BoxCollider2D _squareCollider;
        private PolygonCollider2D _triangleCollider;
        private CircleCollider2D _circleCollider;
        private Rigidbody2D _rigidbody;
        private bool _smashing;
        private Vector3 _horizontalDirection = Vector3.right;
        private Collider2D _currentCollider;
        private int _maxJumpCount = 2;
        private int _currentJumpCount = 0;

        private Shape _currentShape = Shape.Square;
        public Tutorial SpaceBarControl;
        public Tutorial DoubleJumpControl;

        public Sprite Square;
        public Sprite Triangle;
        public Sprite Circle;

        public Sprite RedSpaceBar;
        public Sprite GreySpaceBar;

        private bool _transformTriangleGained;
        private bool _transformCircleGained;
        private bool _doubleJumpGained;

        public Shape CurrentShape
        {
            get { return _currentShape; }
        }

        public AudioClip JumpSfx;
        public AudioClip DoubleJumpSfx;
        public AudioClip SmashAttack;

        public int CurrentCheckpoint { get; private set; }

        private Vector3 _currentCheckpointPosition;
        private SpriteRenderer _currentCheckFlag;

        public Rigidbody2D RigidBody
        {
            get { return _rigidbody; }
        }

        private Vector2 Speed
        {
            get { return _currentShape == Shape.Circle ? _circleSpeed : _normalSpeed; }
        }

        private float CurrentVelocity
        {
            get { return Math.Abs(_rigidbody.velocity.x) + Math.Abs(_rigidbody.velocity.y); }
        }

        private float SpinningVelocity
        {
            get { return Math.Abs(_rigidbody.angularVelocity); }
        }

        // Use this for initialization
        // ReSharper disable once UnusedMember.Local
        void Start()
        {
            Stats.StartNewGame();

            _spriteRenderer = GetComponent<SpriteRenderer>();
            _squareCollider = GetComponent<BoxCollider2D>();
            _triangleCollider = GetComponent<PolygonCollider2D>();
            _circleCollider = GetComponent<CircleCollider2D>();
            _rigidbody = GetComponent<Rigidbody2D>();

            _currentCollider = _squareCollider;
        }

        // Update is called once per frame
        // ReSharper disable once UnusedMember.Local
        void Update()
        {
            ClearJumpCountIfGrounded();
            ProcessMovePlayer();
            ProcessSpecialMove();
            ProcessChangeShape();
            ProcessRespawn();
        }

        private void ClearJumpCountIfGrounded()
        {
            if (Physics2D.Raycast(transform.position, Vector2.down, 0.5f, IgnoreCheckpointLayers))
            {
                // Character is on the ground
                _currentJumpCount = 0;
            }
        }

        private static int IgnoreCheckpointLayers
        {
            get { return ~((1 << 8) + (1 << 10)); }
        }

        private static int IgnoreInteractiveLayers
        {
            get { return ~((1 << 8) + (1 << 9) + (1 << 10)); }
        }

        private void ProcessRespawn()
        {
            if (Input.GetKeyDown(KeyCode.Backspace))
            {
                ReturnToLastCheckpoint();
            }
        }

        public void ReturnToLastCheckpoint()
        {
            if (CurrentCheckpoint > 0)
            {
                Stats.IncreaseRestoredCount();
                _rigidbody.velocity = Vector2.zero;
                _rigidbody.angularVelocity = 0f;
                transform.position = _currentCheckpointPosition;
            }
        }

        private void ProcessSpecialMove()
        {
            if (Input.GetKeyDown(KeyCode.Space))
            {
                switch (_currentShape)
                {
                    case Shape.Triangle:
                        Smash();
                        break;
                    case Shape.Circle:
                        Jump();
                        break;
                }
            }
        }

        private void Smash()
        {
            if (_smashing)
            {
                return;
            }

            AudioSource.PlayClipAtPoint(SmashAttack, transform.position);
            _rigidbody.AddForce(_horizontalDirection * 0.3f);
            _rigidbody.AddForce(Vector3.up * 0.1f);
            StartCoroutine(RotateTriangle());
        }

        private IEnumerator RotateTriangle()
        {
            _smashing = true;

            Quaternion fromAngle = transform.rotation;
            Quaternion toAngle = _horizontalDirection == Vector3.right ?
                Quaternion.Euler(transform.eulerAngles - new Vector3(0, 0, 120)) :
                Quaternion.Euler(transform.eulerAngles + new Vector3(0, 0, 120));

            for (var i = 0f; i < 1; i += Time.deltaTime / 0.4f)
            {
                transform.rotation = Quaternion.Lerp(fromAngle, toAngle, i);
                yield return null;
            }

            _smashing = false;
        }

        private void Jump()
        {
            if (_currentJumpCount < 1 || (_doubleJumpGained && _currentJumpCount < _maxJumpCount))
            {
                _currentJumpCount++;
                _rigidbody.AddForce(Vector2.up * 0.4f);

                AudioSource.PlayClipAtPoint(_currentJumpCount == 1 ? JumpSfx : DoubleJumpSfx, transform.position);
            }
        }

        private void ProcessMovePlayer()
        {
            Vector3 horizontalMotion;

            if (Input.GetKey(KeyCode.RightArrow))
            {
                _horizontalDirection = Vector3.right;
                horizontalMotion = Speed;
            }
            else if (Input.GetKey(KeyCode.LeftArrow))
            {
                _horizontalDirection = Vector3.left;
                horizontalMotion = Speed * -1;
            }
            else
            {
                return;
            }

            if (horizontalMotion.x > 0 && _rigidbody.velocity.x < 0)
            {
                horizontalMotion = CalculateVelocityChange(horizontalMotion);
            }
            else if (horizontalMotion.x < 0 && _rigidbody.velocity.x > 0)
            {
                horizontalMotion = CalculateVelocityChange(horizontalMotion);
            }

            // check for collision in that direction
            Vector3 newPosition = transform.position + horizontalMotion;

            float xMin = _currentCollider.bounds.min.x;
            float xMax = _currentCollider.bounds.max.x;

            var boundPoint = horizontalMotion.x > 0 ?
                new Vector2(xMax + horizontalMotion.x, newPosition.y) :
                new Vector2(xMin + horizontalMotion.x, newPosition.y);

            Debug.DrawLine(transform.position, boundPoint);

            if (!Physics2D.Linecast(transform.position, boundPoint, IgnoreInteractiveLayers))
            {
                transform.position += horizontalMotion;
            }
        }

        private Vector3 CalculateVelocityChange(Vector3 rightMotion)
        {
            float difference = _rigidbody.velocity.x;
            _rigidbody.velocity = new Vector3(_rigidbody.velocity.x + rightMotion.x, _rigidbody.velocity.y);

            rightMotion = Math.Abs(rightMotion.x) > Math.Abs(difference)
                ? new Vector3(rightMotion.x + difference, 0f)
                : Vector3.zero;
            return rightMotion;
        }

        private void ProcessChangeShape()
        {
            if (Input.GetKeyDown(KeyCode.E))
            {
                ChangeShape(Shape.Square, Square, _currentCollider, _squareCollider);

                SpaceBarControl.Hide();
                DoubleJumpControl.Hide();
            }
            else if (Input.GetKeyDown(KeyCode.W) && _transformTriangleGained)
            {
                ChangeShape(Shape.Triangle, Triangle, _currentCollider, _triangleCollider);

                SpaceBarControl.TransformShape.sprite = RedSpaceBar;
                SpaceBarControl.Display();
                
                DoubleJumpControl.Hide();
            }
            else if (Input.GetKeyDown(KeyCode.Q) && _transformCircleGained)
            {
                ChangeShape(Shape.Circle, Circle, _currentCollider, _circleCollider);
                
                SpaceBarControl.TransformShape.sprite = GreySpaceBar;
                SpaceBarControl.Display();

                if (_doubleJumpGained)
                {
                    DoubleJumpControl.Display();
                }
            }
        }

        private void ChangeShape(Shape newShape, Sprite newSprite, Collider2D oldCollider, Collider2D newCollider)
        {
            _currentShape = newShape;
            _spriteRenderer.sprite = newSprite;
            oldCollider.enabled = false;
            newCollider.enabled = true;

            _currentCollider = newCollider;

            _rigidbody.mass = _currentShape == Shape.Square ? 1 : 0.001f;
        }

        public void OnCollisionEnter2D(Collision2D collision)
        {
            if (collision.gameObject.tag == "Smashable" && AbleToSmash(collision))
            {
                collision.gameObject.GetComponent<Smashable>().DestroyBlock();
            }
            else if (collision.gameObject.tag == "Slope" && _currentShape == Shape.Circle)
            {
                _rigidbody.velocity = new Vector2(_rigidbody.velocity.x * 1.4f, _rigidbody.velocity.y * 1.6f);
            }
            else if (collision.gameObject.tag == "Bumper")
            {
                Vector2 bounceDirection = CalculateBounceDirection(collision);
                _rigidbody.AddForce(bounceDirection);
            }
        }

        public void ProcessPickup(PowerUps pickup)
        {
            switch (pickup)
            {
                case PowerUps.TransformTriangle:
                    _transformTriangleGained = true;
                    break;
                case PowerUps.TransformCircle:
                    _transformCircleGained = true;
                    break;
                case PowerUps.DoubleJump:
                    _doubleJumpGained = true;
                    break;
            }
        }

        private static Vector3 CalculateBounceDirection(Collision2D collision)
        {
            return collision.gameObject.transform.up * 0.6f;
        }

        public bool AbleToSmash(Collision2D collision)
        {
            return (_currentShape == Shape.Triangle && (_smashing || RelativeVelocity(collision) > 25));
        }

        private static float RelativeVelocity(Collision2D collision)
        {
            return Math.Abs(collision.relativeVelocity.x) + Math.Abs(collision.relativeVelocity.y);
        }

        public void SetNewCheckpoint(int checkpointNumber, Vector3 position, SpriteRenderer checkFlag)
        {
            if (checkpointNumber > CurrentCheckpoint)
            {
                CurrentCheckpoint = checkpointNumber;
                _currentCheckpointPosition = position;

                if (_currentCheckFlag != null)
                {
                    _currentCheckFlag.color = new Color(1, 1, 1, 0.25f);
                }

                _currentCheckFlag = checkFlag;
                checkFlag.color = new Color(1, 1, 1, 1);
            }
        }
    }
}
