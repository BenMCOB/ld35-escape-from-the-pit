﻿using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class PowerUpPickUp : MonoBehaviour
    {
        public PowerUps Power;
        public Tutorial[] TutorialControls;

        public void OnTriggerEnter2D(Collider2D collision)
        {
            if (collision.gameObject.tag == "Player")
            {
                PlayerController player = collision.gameObject.GetComponent<PlayerController>();
                
                player.ProcessPickup(Power);

                foreach (Tutorial tutorialShape in TutorialControls)
                {
                    if (tutorialShape.gameObject.name.Equals("SpaceControl"))
                    {
                        continue;
                    }

                    if (tutorialShape.gameObject.name.Equals("DoubleJumpControl")
                        && player.CurrentShape != Shape.Circle)
                    {
                        continue;
                    }

                    tutorialShape.Display();
                }

                Destroy(gameObject);
            }
        }
     }
}
