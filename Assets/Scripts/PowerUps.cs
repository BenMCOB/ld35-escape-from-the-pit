﻿namespace Assets.Scripts
{
    public enum PowerUps
    {
        TransformTriangle,
        TransformCircle,
        DoubleJump
    }
}
