﻿namespace Assets.Scripts
{
    public enum Shape
    {
        Circle,
        Square,
        Triangle
    }
}