﻿using UnityEngine;

namespace Assets.Scripts
{
    public class Smashable : MonoBehaviour
    {
        public AudioClip DestroySound;

        public void Start()
        {
            DestroySound = GetComponent<AudioSource>().clip;
        }

        public void DestroyBlock()
        {
            AudioSource.PlayClipAtPoint(DestroySound, transform.position);
            Destroy(gameObject);
        }
    }
}
