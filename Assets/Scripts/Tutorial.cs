﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts
{
    public class Tutorial : MonoBehaviour
    {
        public Image TransformShape;
        public Text TransformLetter;

        public void Display()
        {
            TransformShape.enabled = true;
            TransformLetter.enabled = true;
        }

        public void Hide()
        {
            TransformShape.enabled = false;
            TransformLetter.enabled = false;
        }
    }
}
