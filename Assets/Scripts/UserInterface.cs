﻿using UnityEngine;

namespace Assets.Scripts
{
    public class UserInterface : MonoBehaviour
    {
        public GameObject MenuPanel;
        private bool _inGame;
        private bool _inTitle;
        private bool _inEnd;

        public void Start()
        {
            _inTitle = Application.loadedLevel == 0;
            _inGame = Application.loadedLevel == 1;
            _inEnd = Application.loadedLevel == 2;
        }

        public void Update()
        {
            if (_inGame && MenuPanel != null)
            {
                if (Input.GetKeyDown(KeyCode.Escape))
                {
                    MenuPanel.SetActive(!MenuPanel.activeSelf);
                }

                if (MenuPanel.activeSelf)
                {
                    if (Input.GetKeyDown(KeyCode.Return))
                    {
                        BeginGame_Click();
                    }

                    if (Input.GetKeyDown(KeyCode.X) && MenuPanel.activeSelf)
                    {
                        QuitGame_Click();
                    }
                }
            }

            if (_inTitle)
            {
                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
                {
                    BeginGame_Click();
                }
            }

            if (_inEnd)
            {
                if (Input.GetKeyDown(KeyCode.X))
                {
                    QuitGame_Click();
                }

                if (Input.GetKeyDown(KeyCode.Space) || Input.GetKeyDown(KeyCode.Return))
                {
                    BeginGame_Click();
                }
            }
        }

        public void BeginGame_Click()
        {
            Application.LoadLevel("Game");
        }

        public void EndGame_Click()
        {
            Application.LoadLevel("EndScreen");
        }

        public void QuitGame_Click()
        {
            Application.Quit();
        }
    }
}
